/*
 * serial.h
 *
 * Created: 2013-03-29 16:33:58
 *  Author: roland
 */ 


#ifndef SERIAL_H_
#define SERIAL_H_

typedef void(*recv_callback)(const char);

void serial_setup();

void serial_send_char(const char data);

void serial_send_string(const char* data);

void serial_set_recv_callback(recv_callback callback);

#endif /* SERIAL_H_ */