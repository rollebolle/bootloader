/*
 * bootloader.c
 *
 * Created: 2013-03-01 20:46:10
 *  Author: roland
 */ 


#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#define F_CPU 7372800UL
#include <util/delay.h>

#include "flash.h"
#include "common.h"
#include "fs.h"
#include "serial.h"

static bool programming = false;
static bool  cluster_complete = false;
static uint32_t cluster_ptr = 0;
static uint32_t prog_size = 0;
static uint8_t prog_data_block[CLUSTER_SIZE];
static uint8_t ready_data_block[CLUSTER_SIZE];
static char command[64];
static uint32_t command_size = 0;	


/*

 TODO: do so that upload program can take more than 1k progs


*/

void ls()
{
	serial_send_string("file table:\r\n");
	for (int i = 0; i < DISK_SIZE / CLUSTER_SIZE; ++i)
	{		
		struct file_table_entry fe;
		read_file_table_entry(i, &fe);
		if (fe.file_start == true)
		{
			char buf[64];
			sprintf(buf, "  - program: %s\r\n", fe.name);
			serial_send_string(buf);
		}
		
	}
}

void program(const char* name, uint32_t size)
{	
	prog_size = 0;
	programming = true;	
	
	uint8_t slots[64];
	uint8_t slots_written = 0;
	write_program_entry(name, size, slots);
	serial_send_string("waiting for data\r\n");
	
	
	while (prog_size < size)
	{
		_delay_ms(10);		
		if (cluster_complete)			
		{			
			write_program_cluster(slots[slots_written++], ready_data_block, CLUSTER_SIZE);
			cluster_complete = false;
		}			
	}
	// always end with a write for the last cluster
	memcpy(ready_data_block, prog_data_block, CLUSTER_SIZE);
	write_program_cluster(slots[slots_written++], ready_data_block, MIN(CLUSTER_SIZE, cluster_ptr));
	
	
	serial_send_string("finished\r\n");
	serial_send_string("\r\n");		
	programming = false;	
	
}

bool handle_command()
{
	if (command[command_size - 1] != '\r')
		return false;
	
	command[command_size - 1] = '\0';
	//serial_send_string("received command: ");
	//serial_send_string(command);
	serial_send_string("\r\n");
	
	if (!memcmp("ls", command, 2))
	{
		ls();
		
	}	
	else if (!memcmp("run ", command, 4))
	{
		serial_send_string("running program\r\n");		
		if (run_program(command + 4) == -1)
			serial_send_string("could not find program ");
	}
	else if (!memcmp("rm ", command, 3))
	{
		remove_program(command + 3);
	}
	else if (!memcmp("prog ", command, 5)) // syntax: orig "myapp" 0x0200 (name, size)
	{
		char* name;
		uint32_t size;
		name = strtok((char*)command + 5, " ");
		size = atoi(strtok(NULL, " "));
		if (size > check_free_space())
			serial_send_string("not enough space for program");
		else
			program(name, size);
			
	}
	else
		serial_send_string("invalid command\r\n");
	
	command_size = 0;
	return true;
}


void on_receive(const char data)
{
	if (!programming)
	{
		serial_send_char(data);		
		command[command_size] = data;
		command_size++;		
	}
	else
	{
		prog_data_block[cluster_ptr] = data;
		prog_size++;
		cluster_ptr++;
		if (cluster_ptr == CLUSTER_SIZE)
		{
			// will trigger a flash write
			memcpy(ready_data_block, prog_data_block, CLUSTER_SIZE);
			cluster_ptr = 0;
			cluster_complete = true;			
		}
		
		serial_send_char('.');
	}
}

void print_prompt()
{
	serial_send_string("\r\nboot>\0");
}

int main(void)
{			
	MCUCR = (1<<IVCE);
	MCUCR = (1<<IVSEL);
	
	serial_setup();		
	serial_set_recv_callback(on_receive);
	
	print_prompt();
	int i = 0;
    while(1)
    {
		++i;
		_delay_ms(100);		

		if (handle_command())
			print_prompt();
    }
}