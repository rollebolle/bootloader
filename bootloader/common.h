/*
 * common.h
 *
 * Created: 2013-03-06 19:16:04
 *  Author: roland
 */ 


#ifndef COMMON_H_
#define COMMON_H_

typedef uint8_t bool;
#define true 1
#define false 0

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#endif /* COMMON_H_ */