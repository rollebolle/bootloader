/*
 * flash.c
 *
 * Created: 2013-03-06 19:14:50
 *  Author: roland
 */ 

#include <avr/boot.h>
#include <stdint.h>
#include <avr/interrupt.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "flash.h"


void write_page_to_flash(uint32_t address, const uint8_t* buf, size_t size, bool keep_data)
{
	uint8_t sreg;
	sreg = SREG;
	
	uint8_t page_buf[SPM_PAGESIZE];
	const uint8_t* data_ptr;
	if (keep_data)
	{
		data_ptr = page_buf;
		uint32_t address_offset = address % SPM_PAGESIZE;
		
		for (int i = 0; i < SPM_PAGESIZE; ++i)
			page_buf[i] = pgm_read_byte_near(address - address_offset + i);
		
		memcpy(page_buf + address_offset, buf, size);
		
		address -= address_offset;
		size = SPM_PAGESIZE;
	}
	else
		data_ptr = buf;
	
	cli();
	eeprom_busy_wait ();
	boot_page_erase(address);
	boot_spm_busy_wait();
	for (int i = 0; i < size; i += 2)
	{
		uint16_t w = *data_ptr++;
		w += (*data_ptr++) << 8;
		boot_page_fill(address + i, w);
	}
	
	boot_page_write(address);
	boot_spm_busy_wait();
	
	boot_rww_enable();
	SREG = sreg;
}

void write_to_flash(uint32_t address, uint8_t* buf, size_t size)
{
	size_t written = 0;
	
	uint32_t address_to_write = address;
	uint8_t* p = buf;
	size_t to_write = 0;
	
	while (written < size)
	{
		if (size - written > SPM_PAGESIZE)
		to_write = SPM_PAGESIZE;
		else
		to_write = size - written;
		
		write_page_to_flash(address_to_write, p, to_write, false);
		p += SPM_PAGESIZE;
		written += SPM_PAGESIZE;
		address_to_write += written;
	}
	
}
