/*
 * fs.c
 *
 * Created: 2013-03-17 08:42:33
 *  Author: rwn
 */ 

#include <avr/pgmspace.h>
#include <string.h>
#include <avr/interrupt.h>
#include <avr/boot.h>

#include "serial.h"
#include "fs.h"
#include "flash.h"

void read_file_table_entry(uint16_t slot, struct file_table_entry* fe)
{
	uint8_t buf[FILE_TABLE_ENTRY_SIZE];
	for (size_t j = 0; j < FILE_TABLE_ENTRY_SIZE; j++)
	{
		buf[j] = pgm_read_byte_near(FILE_TABLE_START + slot*FILE_TABLE_ENTRY_SIZE + j);
	}
	memcpy(fe, buf, FILE_TABLE_ENTRY_SIZE);
}

static int16_t find_free_program_slot(uint16_t begin_slot)
{
	struct file_table_entry fe;
	for (uint16_t i = begin_slot; i < DISK_SIZE/CLUSTER_SIZE; ++i)
	{
		read_file_table_entry(i , &fe);
		if (fe.name[0] == 0xff)
		return i;
	}
	return -1;
}

static int16_t find_next_slot(uint16_t current_slot)
{
	struct file_table_entry fe;
	read_file_table_entry(current_slot, &fe);
	return fe.next;
}


static uint8_t write_file_table_entries(uint16_t begin_slot, struct file_table_entry* fe, bool delete, uint8_t* slot_allocations)
{
	int16_t size_left = fe->size;
	uint16_t slot = begin_slot;
	uint16_t next_slot = 0;
	uint8_t slots_written = 0;
	while (size_left > 0)
	{
		size_left -= CLUSTER_SIZE;
		if (size_left > 0)
		{
			next_slot = find_free_program_slot(slot + 1);
			fe->next = next_slot;
		}
		else
			fe->next = -1;
		
		if (slot == begin_slot && !delete)
			fe->file_start = true;
		else
			fe->file_start = false;
		
		write_page_to_flash(slot*FILE_TABLE_ENTRY_SIZE + FILE_TABLE_START, (uint8_t*)fe, FILE_TABLE_ENTRY_SIZE, true);
		if (!delete)
			slot_allocations[slots_written] = slot;
		slots_written++;
		slot = next_slot;
	}
	return slots_written;
}


static int16_t find_program_first_slot(const char* name, struct file_table_entry* fe)
{
	for (int i = 0; i < DISK_SIZE / CLUSTER_SIZE; ++i)
	{
		read_file_table_entry(i, fe);
		if (!strcmp(fe->name, name))
			return i;
	}
	return -1;
}


int8_t write_program_cluster(uint16_t slot, const uint8_t* cluster, size_t size)
{
	serial_send_string("writing cluster\r\n");
	int32_t written = 0;	
	uint32_t address = FIRST_PROGRAM_ENTRY + slot*CLUSTER_SIZE;
	while ((int32_t)size - written > 0)
	{
		write_page_to_flash(address + written, cluster + written, MIN(SPM_PAGESIZE, (int32_t)size - written), true);		
		written += SPM_PAGESIZE;		
	}
	return 0;
}

void jump_to_start()
{
	cli();
	MCUCR = (1 << IVCE);
	MCUCR = 0;
	void (*prg)( void ) = 0x0;
	prg();
}


uint32_t check_free_space()
{
	uint32_t free_space = 0;
	
	struct file_table_entry fe;
	for (uint16_t i = 0; i < DISK_SIZE/CLUSTER_SIZE; ++i)
	{
		read_file_table_entry(i , &fe);
		if (fe.name[0] == 0xff)
		free_space += CLUSTER_SIZE;
	}
	return free_space;
}


void remove_program(const char* name)
{			
	struct file_table_entry fe;
	int16_t slot = find_program_first_slot(name, &fe);	
	int16_t next_slot = slot;
	while (next_slot != -1)
	{	
		slot = next_slot;
		next_slot = find_next_slot(slot);
		fe.file_start = false;
		fe.name[0] = 0xff; // this is what marks slot as empty
		fe.size = 1;
		write_file_table_entries(slot, &fe, true, 0);
	}
}

int8_t run_program(const char* name)
{	
	struct file_table_entry fe;
	int16_t slot = find_program_first_slot(name, &fe);
	if (slot == -1)			
		return -1;
					
	// copy to 0x0
	int32_t total_written = 0;
	int32_t slot_written = 0;
	while ((int32_t)fe.size - total_written > 0)
	{
		uint8_t buf[SPM_PAGESIZE];
		for (int byte = 0; byte < SPM_PAGESIZE; ++byte)		
			buf[byte] = pgm_read_byte_near(FIRST_PROGRAM_ENTRY + slot*CLUSTER_SIZE + byte + total_written);
		write_to_flash(0x0 + total_written, buf, SPM_PAGESIZE);
		total_written += SPM_PAGESIZE;
		slot_written += SPM_PAGESIZE;
		if 	(slot_written >= CLUSTER_SIZE)
		{
			slot = find_next_slot(slot);
			slot_written = 0;
		}
	}
	
	serial_send_string("jumping to start");
	jump_to_start();
	
	return 0;
}


int8_t write_program_entry(const char* name, size_t size, uint8_t* slot_allocations)
{
	// Write program entry
	int16_t slot = find_free_program_slot(0);
	struct file_table_entry fe;
	strcpy(fe.name, name);
	fe.size = size;
	int8_t slots = write_file_table_entries(slot, &fe, false, slot_allocations);
	serial_send_string("file table written");
	
	// Write program
	//write_program_code(slot, program, size);
	
	return slots;
}