/*
 * fs.h
 *
 * Created: 2013-03-17 08:42:43
 *  Author: rwn
 */ 


#ifndef FS_H_
#define FS_H_

#include <stdint.h>
#include <stdlib.h>
#include "common.h"


#ifndef BOOTSECTOR_START
#define BOOTSECTOR_START 0xE000
#endif 

#ifndef FIRST_PROGRAM_ENTRY
#define FIRST_PROGRAM_ENTRY 0x4000
#endif

#define LAST_PROGRAM_ENTRY (BOOTSECTOR_START - 0x400)
#define CLUSTER_SIZE 0x400 // 1024
#define DISK_SIZE (LAST_PROGRAM_ENTRY - FIRST_PROGRAM_ENTRY)
#define FILE_TABLE_SIZE (sizeof(struct file_table_entry) * (DISK_SIZE / CLUSTER_SIZE))
#define FILE_TABLE_START (BOOTSECTOR_START - FILE_TABLE_SIZE - 0x100)


struct file_table_entry
{
	bool file_start;
	char name[8];
	uint16_t size;
	uint16_t next;
};

#define FILE_TABLE_ENTRY_SIZE sizeof(struct file_table_entry)

void jump_to_start();

void read_file_table_entry(uint16_t slot, struct file_table_entry* fe);

uint32_t check_free_space();

void remove_program(const char* name);

int8_t run_program(const char* name);

// writes the program entry in the file table and returns how many were needed, and their numbers in slot_allocations
int8_t write_program_entry(const char* name, size_t size, uint8_t* slot_allocations);

int8_t write_program_cluster(uint16_t slot, const uint8_t* cluster, size_t size);


#endif /* FS_H_ */