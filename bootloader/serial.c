/*
 * serial.c
 *
 * Created: 2013-03-29 16:34:44
 *  Author: roland
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "serial.h"
#include "common.h"

#ifndef F_CPU
#define F_CPU 7372800UL
#endif

#define USART_BAUDRATE 9600
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)


static recv_callback on_receive = 0;

void serial_setup()
{
	// Enable rx and tx
	UCSR0B	|= (1 << RXEN0) | (1 << TXEN0);
	// set 8 bit data length
	
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
	UCSR0B |= (1 << RXCIE0);
	// setup speed
	UBRR0H = (BAUD_PRESCALE >> 8);
	UBRR0L = BAUD_PRESCALE;
	
	sei();
}


void serial_send_char(const char data)
{
	while (!(UCSR0A & (1 << UDRE0)))
	;
	UDR0 = data;
}

void serial_send_string(const char* data)
{
	const char* ptr = data;
	while (*ptr	!= '\0')
	{
		serial_send_char(ptr[0]);
		ptr++;		
	}
}

void serial_set_recv_callback(recv_callback callback)
{
	on_receive = callback;	
}

ISR(USART0_RX_vect)
{
	unsigned char recv_byte;
	recv_byte = UDR0;
	//send_char(recv_byte);
	if (on_receive)
		on_receive(recv_byte);
}