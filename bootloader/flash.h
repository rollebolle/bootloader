/*
 * program_memory.h
 *
 * Created: 2013-03-06 19:13:45
 *  Author: roland
 */ 


#ifndef PROGRAM_MEMORY_H_
#define PROGRAM_MEMORY_H_

#include "common.h"

void write_page_to_flash(uint32_t address, const uint8_t* buf, size_t size, bool save_data);

void write_to_flash(uint32_t address, uint8_t* buf, size_t size);


#endif /* PROGRAM_MEMORY_H_ */