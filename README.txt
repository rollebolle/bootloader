*** Multi-program Bootloader for AVR microprocessors with serial interface ***

This bootloader lets you program multiple programs and choose which one to run at boot using
the serial line. The serial prompt that lets you upload, erase and list programs. It splits
the flash into two; one section for the current program (beginning at 0x0), one to store the 
programs. It uses a FAT-like file system on the flash, located just below the boot sector.

Only tested on atmega644 so far. As the bootloader is ~5k in size, you need a big-flash device.
Program BOOTRST fuse to use. There are 3 defines you have to set  to build the project: 
(or use the defaults for an atmega644):
  - -mmcu, for the processor type
  - F_CPU, for the serial baud prescaler
  - BOOTSECTOR_START, depending on your BOOTSZ fuses, set this accordingly (in byte address)
  - FIRST_PROGRAM_ENTRY, decides where the split between active program/stored programs should
    be. For example, if you set it to 0x4000 (16384), the max size for each program will be 16k,
    while the rest (around 40k for an atmega644) will be used to store programs.


Commands:
 - prog <name> <size> Begins a programming sequence of the max 8-byte named program "name" with 
   the size "size". Data should be a bin file. If using for example Realterm to upload, set a
   small byte delay. Will exit routine when "size" bytes have been received
 - ls Lists all programs
 - remove <name> Removes the program with name "name"
 - run <name> Runs the program with name "name" 


Author: Roland Waltersson